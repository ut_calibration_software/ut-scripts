/*
 *  Created on: 02.2024
 *  Author: Wojciech Krupa (wokrupa@cern.ch)
 *  based on code P.Gandini
 */

{

#include <iostream>
#include <string.h>
#include <vector>

  using namespace std;
  TCanvas c0("c0", "c0", 600, 600);
  TFile outputFile("overlay_center_natural_UTbV.root", "RECREATE");
  double xmin = -9;
  double xmax = 9;
  double xstep = 1;
  double ymin = -7;
  double ymax = 7;
  double ystep = 1;
  double radiusx = 0.2;
  double radiusy = 0.2;
  int lateralbox = 0;

  TGraph *diagon = new TGraph(0);
  diagon->SetPoint(0, xmin, ymin);
  diagon->SetPoint(1, xmax, ymax);
  diagon->SetTitle("");
  diagon->SetName("");
  diagon->SetLineColor(kWhite);
  diagon->Draw("AL");
  double xxx = -999;
  double yyy = -999;

  for (int j = 0; xxx < xmax; j++)
  {
    xxx = xmin + xstep * j;
    TLine *gray0 = new TLine(xxx, ymin, xxx, ymax);
    gray0->Draw("L");
    gray0->Write();
    gray0->SetLineColor(kGray);
    gray0->SetLineStyle(1);
  }

  for (int j = 0; yyy < ymax; j++)
  {
    yyy = ymin + ystep * j;
    TLine *gray0 = new TLine(xmin, yyy, xmax, yyy);
    gray0->Draw("L");
    gray0->Write();
    gray0->SetLineColor(kGray);
    gray0->SetLineStyle(1);
  }

  // start populating
  TLine *linein0 = new TLine(-2, 2, 2, 2);
  linein0->Draw("L");
  linein0->Write();
  TLine *linein1 = new TLine(-2, -2, 2, -2);
  linein1->Draw("L");
  linein1->Write();
  TLine *linein2 = new TLine(-2, -2, -2, 2);
  linein2->Draw("L");
  linein2->Write();
  TLine *linein3 = new TLine(2, -2, 2, 2);
  linein3->Draw("L");
  linein3->Write();
  TLine *linein4 = new TLine(-1, 1, 1, 1);
  linein4->Draw("L");
  linein4->Write();
  TLine *linein5 = new TLine(-1, -1, 1, -1);
  linein5->Draw("L");
  linein5->Write();
  TLine *linein6 = new TLine(-1, -1, -1, 1);
  linein6->Draw("L");
  linein6->Write();
  TLine *linein7 = new TLine(1, -1, 1, 1);
  linein7->Draw("L");
  linein7->Write();
  TLine *linein8 = new TLine(-1, 0.5, 1, 0.5);
  linein8->Draw("L");
  linein8->Write();
  TLine *linein9 = new TLine(-1, -0.5, 1, -0.5);
  linein9->Draw("L");
  linein9->Write();

  TEllipse *circle = new TEllipse(0, 0, radiusx, radiusy);
  ;
  circle->SetLineColor(kGray + 2);
  circle->SetFillStyle(1001);
  circle->SetFillColor(kWhite);
  circle->SetLineWidth(1);
  circle->Draw("L");
  circle->Write();

  TBox *boxL = new TBox(xmin, ymin, xmin + 1, ymax);
  boxL->SetFillColor(kGray);
  boxL->SetFillStyle(3001);
  TBox *boxR = new TBox(xmax - 1, ymin, xmax, ymax);
  boxR->SetFillColor(kGray);
  boxR->SetFillStyle(3001);
  if (lateralbox == 1)
  {
    boxL->Draw("");
    boxL->Write();
    boxR->Draw("");
    boxR->Write();
  }

  TLine *back0 = new TLine(xmin, ymin, xmin, ymax);
  TLine *back1 = new TLine(xmin, ymax, xmax, ymax);
  TLine *back2 = new TLine(xmax, ymax, xmax, ymin);
  TLine *back3 = new TLine(xmax, ymin, xmin, ymin);
  back0->Draw("L");
  back1->Draw("L");
  back2->Draw("L");
  back3->Draw("L");
  back0->SetLineWidth(1);
  back1->SetLineWidth(1);
  back2->SetLineWidth(1);
  back3->SetLineWidth(1);
  back0->Write();
  back1->Write();
  back2->Write();
  back3->Write();
  TLine *line0 = new TLine(xmin, 0, xmax, 0);
  TLine *line1 = new TLine(0, ymin, 0, ymax);
  line0->SetLineWidth(1);
  line1->SetLineWidth(1);
  line0->Draw("L");
  line1->Draw("L");
  line0->Write();
  line1->Write();

  gStyle->SetLineWidth(0); // Make box edges invisible

  // recoverable - gray + 1
  std::vector<std::pair<double, double>> gray_1_boxes = {{-5, 2}, {-5, -3}}; // fails pll_clk_cfg & deserialiser scan, //
  std::vector<std::tuple<double, double, double>> gray_1_boxes_1ASIC = {
      {-7, -2, 0}, {-7, -2, 1}, // , asic 0&1 failed deserialiser scan - one bad e-link
      {-6, 2, 3},               // UTbV_6AT_M2_0 - ASIC disabled
      {-5, -1, 1},              // UTbV_5AB_M1_2 - ASIC disabled
      {8, -1, 1},
      {8, -1, 3}, // UTbV_9CB_M_1/3 - ASIC disabled
      {-5, 4, 1}, //
      
  };

  std::vector<std::tuple<double, double, double>> gray_1_half_boxes_1ASIC = {
      {-1, -2, 4}, // UTbV_1AB_S2E_0
      {1, -1, 4},  // UTbV_1AB_M1E_1/3
      {1, -2, 4},
      {1, -2, 5}, // UTbV_2CB_S1W_2/3
  };
  std::vector<std::tuple<double, double, double>> gray_1_doublehalf_boxes_1ASIC = {};

  // dead modules - black
  std::vector<std::pair<double, double>> boxes = {};
  std::vector<std::pair<double, double>> black_boxes = {{1, 3}, {-5, -7}, {-3, -6}, {-6, 1}, {-5, 2}}; // dead (powering)
  std::vector<std::pair<double, double>> black_half_boxes = {{1, 1}};                                  // dead (powering)
  std::vector<std::tuple<double, double, double>> black_boxes_1ASIC = {{8, 1, 0}};                     // , asic 0&1 failed deserialiser scan - one bad e-link

  for (const auto &box : boxes)
  {
    double x = box.first;
    double y = box.second;
    TBox *grayBox = new TBox(x, y, x + xstep, y + ystep);
    grayBox->SetFillColor(kGray);
    grayBox->Draw("same");
    grayBox->Write();
  }

  for (const auto &box : black_boxes)
  {
    double x = box.first;
    double y = box.second;
    TBox *grayBox = new TBox(x, y, x + xstep, y + ystep);
    grayBox->SetFillColor(kGray + 2);
    grayBox->Draw("same");
    grayBox->Write();
  }

  for (const auto &box : gray_1_boxes)
  {
    double x = box.first;
    double y = box.second;
    TBox *grayBox = new TBox(x, y, x + xstep, y + ystep);
    grayBox->SetFillColor(kGray + 1);
    grayBox->Draw("same");
    grayBox->Write();
  }

  for (const auto &box : black_half_boxes)
  {
    double x = box.first;
    double y = box.second;
    TBox *grayBox = new TBox(x, y, x + xstep / 2, y + ystep);
    grayBox->SetFillColor(kGray + 2);
    grayBox->Draw("same");
    grayBox->Write();
  }

  for (const auto &triple : gray_1_boxes_1ASIC)
  {
    double x, y, asic;
    std::tie(x, y, asic) = triple;
    TBox *grayBox = new TBox(x + xstep / 4 * asic, y, x + xstep / 4 * (1 + asic), y + ystep);
    grayBox->SetFillColor(kGray + 1);
    grayBox->Draw("same");
    grayBox->Write();
  }

  for (const auto &triple : black_boxes_1ASIC)
  {
    double x, y, asic;
    std::tie(x, y, asic) = triple;
    TBox *grayBox = new TBox(x + xstep / 4 * asic, y, x + xstep / 4 * (1 + asic), y + ystep);
    grayBox->SetFillColor(kGray + 2);
    grayBox->Draw("same");
    grayBox->Write();
  }

  for (const auto &triple : gray_1_half_boxes_1ASIC)
  {
    double x, y, asic;
    std::tie(x, y, asic) = triple;
    TBox *grayBox = new TBox(x + xstep / 8 * asic, y, x + xstep / 8 * (1 + asic), y + ystep);
    grayBox->SetFillColor(kGray + 1);
    grayBox->Draw("same");
    grayBox->Write();
  }

  for (const auto &triple : gray_1_doublehalf_boxes_1ASIC)
  {
    double x, y, asic;
    std::tie(x, y, asic) = triple;
    TBox *grayBox = new TBox(x + xstep / 8 * asic, y, x + xstep / 8 * (1 + asic), y + ystep / 2);
    grayBox->SetFillColor(kGray + 1);
    grayBox->Draw("same");
    grayBox->Write();
  }

  c0.Draw();
  c0.Write();
  c0.SaveAs("canvas.png");
  outputFile.Close();
  gSystem->Exit(0);
}
